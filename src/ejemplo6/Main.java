/*
 * Main.java
 *
 * Created on 11 de agosto de 2008, 16:09
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo6;

/**
 *
 * @author alberto
 */
import java.sql.*;

public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
        public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexion con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");

            //Uso del metodo executeQuery
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                      ResultSet.CONCUR_READ_ONLY); //Todo tipo de movimientos con cambios y s�lo lectura
            
            String sql = "select  * from articulos";
            
            rs = stmt.executeQuery(sql);  
            
            System.out.println("Timeout de consulta: "+stmt.getQueryTimeout()+ " sg");
            System.out.println("N�mero m�ximo de registros: "+stmt.getMaxRows());
            System.out.println("Tama�o m�ximo de campo: "+stmt.getMaxFieldSize()+" bytes");
            System.out.println("N�mero de registros devueltos cada vez: "+stmt.getFetchSize());
            
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(stmt!=null)
                    stmt.close();

                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
