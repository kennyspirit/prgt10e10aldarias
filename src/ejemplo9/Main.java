/*
 * Main.java
 *
 * Created on 12 de agosto de 2008, 11:32
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo9;

import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;

    try {
      //Registrando el Driver
      String driver = "com.mysql.jdbc.Driver";
      Class.forName(driver).newInstance();
      System.out.println("Driver " + driver + " Registrado correctamente");

      //Abrir la conexin con la Base de Datos
      System.out.println("Conectando con la Base de datos...");
      String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
      conn = DriverManager.getConnection(jdbcUrl, "paco", "");
      System.out.println("Conexin establecida con la Base de datos...");

      //Comprobar soporte batch
      DatabaseMetaData dm = conn.getMetaData();
      System.out.println("Soporta procesamiento batch -> " + dm.supportsBatchUpdates());

    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try
  }
}
