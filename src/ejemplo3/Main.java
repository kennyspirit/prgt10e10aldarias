/*
 * Main.java
 *
 * Created on 11 de agosto de 2008, 13:07
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo3;

/**
 *
 * @author pacoaldarias
 */
import java.sql.*;

public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conexion = null;
        Statement stmt = null;
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
           
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conexion = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");

            //Creando un objeto statement
            conexion = DriverManager.getConnection(jdbcUrl,"root","");
            stmt = conexion.createStatement();

            String sql = "CREATE TABLE vendedores2 (" +
                    "id int NOT NULL auto_increment," +
                    "nombre varchar(50) NOT NULL default ''," +
                    "fecha_ingreso date NOT NULL default '0000-00-00'," +
                    "salario float NOT NULL default '0'," +
                    "PRIMARY KEY  (id))";
          
            stmt.executeUpdate(sql);

            //A�adimos datos a la tabla vendedores
            sql = "INSERT INTO vendedores2 VALUES (1,'pepe', '2007-01-01', 12000)";
            stmt.executeUpdate(sql);
            
            System.out.println("Registro a�adido!");
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(stmt!=null)
                    stmt.close();                
                if(conexion!=null)
                    conexion.close();
                System.out.println("Conexi�n cerrada");
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
