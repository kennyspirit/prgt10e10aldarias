/*
 * Main.java
 *
 * Created on 7 de septiembre de 2008, 23:38
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo28JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;
import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        JdbcRowSetImpl jrs = null;
        String user = "root";
        String pass = "";
        String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";

        try {
            //Cargamos el driver 
            Registramos_el_driver;
            
            //Creamos el objeto JdbcRowSet e 
            //inicializamos las propiedades de conexi�n
            Instanciamos_la_clase_JdbcRowSetImpl;
            Inicializamos_las_propiedades_de_conexion;
            
            String sql = "select * from facturas where fecha between ? and ?";
            
            Inicializamos_la_sql;
            
            Date fini = new Date(System.currentTimeMillis() - (long)365*86400000);
            Date ffin = new Date(System.currentTimeMillis());
            
            Inicializamos_el_parametro_1;
            Inicializamos_el_parametro_2;
            
            
            System.out.println("Consultado facturas de fechas entre: "+fini + ":"+ ffin);
            
            Ejecutamos_el_RowSet;
            
            //Mostramos los resultados

            Recorremos_el_RowSet_y_Mostramos_los_resultados;
            
        } catch (SQLException se ){
            se.printStackTrace();
        } catch (Exception e ){
            e.printStackTrace();
        } finally {
            try {
                if (jrs != null ) {
                    jrs.close();
                }
                
            } catch (Exception fe) {
                fe.printStackTrace();
            }
        }
    }
    
}
