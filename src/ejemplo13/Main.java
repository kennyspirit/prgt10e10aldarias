/*
 * Main.java
 *
 * Created on 22 de agosto de 2008, 8:51
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo13;
import java.sql.*;
/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
         public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"paco","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            
            //Ejecutamos la SELECT sobre la tabla clientes
            String sql = "select * from clientes";
            
            rs = stmt.executeQuery(sql);
            
            System.out.println("Situamos el cursor al final");
            System.out.println("Modificamos la direcci�n del �ltimo registro");
            rs.last();
            rs.updateString("direccion", "C/ Pepe Ciges, 3");
            rs.updateRow();
            
            //Creamos un nuevo registro en la tabla de clientes
            
            System.out.println("Creamos un nuevo registro");
            rs.moveToInsertRow();
            rs.updateString(2,"Killy Lopez");
            rs.updateString(3,"Wall Street 3674");
            rs.insertRow();

            //Nos situamos al final del ResultSet
            System.out.println("Borramos el �ltimo registro");
            rs.last();
            rs.deleteRow();

            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(rs!=null)
                    rs.close();
                if(stmt!=null)
                    stmt.close();
                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
